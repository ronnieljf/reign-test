import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from '../schemas/user.schema';
import { CommandModule } from 'nestjs-command';
import { UsersSeed } from './users.seed';
@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    CommandModule,
  ],
  providers: [UserService, UsersSeed],
  controllers: [],
  exports: [UserService],
})
export class UserModule {}
