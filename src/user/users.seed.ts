import { Command } from 'nestjs-command';
import { Injectable } from '@nestjs/common';
import { User, UserDocument } from '../schemas/user.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as fs from 'fs';

@Injectable()
export class UsersSeed {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}
  @Command({
    command: 'create:users',
    describe: 'seeder users',
  })
  async create(): Promise<void> {
    const readUsers = fs.readFileSync('src/user/users.json');
    let users = [];
    try {
      users = JSON.parse(readUsers.toString());
    } catch (error) {
      users = [];
    }
    await this.userModel.insertMany(users);
  }
}
