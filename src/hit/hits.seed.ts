import { Command } from 'nestjs-command';
import { Injectable } from '@nestjs/common';
import { Hit, HitDocument } from '../schemas/hit.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as fs from 'fs';

@Injectable()
export class HitsSeed {
  constructor(@InjectModel(Hit.name) private hitModel: Model<HitDocument>) {}
  @Command({
    command: 'create:hits',
    describe: 'seeder hits',
  })
  async create(): Promise<void> {
    const readHits = fs.readFileSync('src/hit/hits.json');
    let hits = [];
    try {
      hits = JSON.parse(readHits.toString());
    } catch (error) {
      hits = [];
    }
    await this.hitModel.insertMany(hits);
  }
}
