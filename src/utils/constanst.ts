export class CONSTANTS {
  static MONTHS = [
    { name: 'JANUARY', number: 1 },
    { name: 'FEBRUARY', number: 2 },
    { name: 'MARCH', number: 3 },
    { name: 'APRIL', number: 4 },
    { name: 'MAY', number: 5 },
    { name: 'JUNE', number: 6 },
    { name: 'JULY', number: 7 },
    { name: 'AUGUST', number: 8 },
    { name: 'SEPTEMBER', number: 9 },
    { name: 'OCTOBER', number: 10 },
    { name: 'NOVEMBER', number: 11 },
    { name: 'DECEMBER', number: 12 },
  ];
}
