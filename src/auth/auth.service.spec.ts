import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule, getModelToken } from '@nestjs/mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { AuthService } from './auth.service';
import { UserService } from '../user/user.service';
import { UserModule } from '../user/user.module';
import { User, UserSchema, UserDocument } from '../schemas/user.schema';
import { factory } from 'fakingoose';
import { Model } from 'mongoose';
import { LoginDTO } from './login.dto';

describe('AuthService', () => {
  let userService: UserService;
  let mongod: MongoMemoryServer;
  let userModel: Model<UserDocument>;
  let email: string;
  let password: string;
  const userFactory = factory<UserDocument>(
    UserSchema,
    {},
  ).setGlobalObjectIdOptions({ tostring: false });
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AuthService, UserService],
      imports: [
        MongooseModule.forRootAsync({
          useFactory: async () => {
            mongod = new MongoMemoryServer();
            await mongod.start();
            const mongoUri = await mongod.getUri();
            return {
              uri: mongoUri,
              ...{},
            };
          },
        }),
        MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
        UserModule,
      ],
    }).compile();
    userService = module.get<UserService>(UserService);
    userModel = module.get<Model<UserDocument>>(getModelToken(User.name));
  });

  beforeEach(async () => {
    const mockUser = userFactory.generate();
    mockUser.email = 'ronnieljf2@gmail.com';
    email = mockUser.email;
    password = mockUser.password;
    return await userModel.create(mockUser);
  });

  it('login user', async () => {
    const loginDTO: LoginDTO = new LoginDTO();
    loginDTO.email = email;
    loginDTO.password = password;
    const res = await userService.findByLogin(loginDTO);
    expect(res).not.toBe(null);
  });

  afterAll(async () => {
    await userModel.deleteMany({});
    await userModel.db.close();
    if (mongod) await mongod.stop();
  });
});
